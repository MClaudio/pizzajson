﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace PizzaApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public Dictionary<string, int> Top20Toppings { get; private set; }

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public IActionResult OnGet()
        {
            var api = new BrightwayApi();
            try
            {
                api.GetPizzaToppings();
                Top20Toppings = api.GetMostOrderedToppingCombinations(20);
            }
            catch(Exception e)
            {
                // TODO: Log exception
                return Redirect("/Error");
            }
            return Page();
        }
    }
}
