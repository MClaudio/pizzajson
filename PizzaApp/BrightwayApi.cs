﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace PizzaApp.Pages
{
    public class BrightwayApi
    {
        private readonly string endpointUrl = "https://www.brightway.com/CodeTests/pizzas.json";

        Dictionary<string, int> _toppingCombinationCount;
        public BrightwayApi()
        {
            _toppingCombinationCount = new Dictionary<string, int>();
        }

        public void GetPizzaToppings()
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(endpointUrl);

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            if (res.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception("API Response Code not OK. Code: " + res.StatusCode);
            }

            StreamReader reader = new StreamReader(res.GetResponseStream());
            string data = reader.ReadToEnd();

            // Deserialize JSON string into list of Pizzas
            List<Pizza> pizzas = JsonSerializer.Deserialize<List<Pizza>>(data);
            
            AggregateToppingCombinations(pizzas);
        }

        private void AggregateToppingCombinations(List<Pizza> pizzas)
        {
            foreach (Pizza pie in pizzas)
            {
                string[] toppingCombo = pie.toppings;

                //Trim outer whitespace and convert to title-case for visual purposes
                toppingCombo = toppingCombo.Select(x => new CultureInfo("en-US").TextInfo.ToTitleCase(x.Trim())).ToArray();

                // Sort array alphabetically 
                // This way topping combos like ['pepperoni', 'mushrooms'] and ['mushrooms', 'pepperoni'] are considered the same
                Array.Sort(toppingCombo, (x, y) => string.Compare(x, y));

                // TODO: Handle case of duplicate toppings. Is that valid? Ex: ['pepperoni', 'pepperoni']

                // Create comma-separated string as dictionary key
                string key = string.Join(", ", toppingCombo);

                if (_toppingCombinationCount.ContainsKey(key))
                {
                    _toppingCombinationCount[key]++;
                }
                else
                {
                    _toppingCombinationCount.Add(key, 1);
                }
            }
        }

        public Dictionary<string, int> GetMostOrderedToppingCombinations(int count = 20)
        {
            return _toppingCombinationCount.OrderByDescending(x => x.Value)
                        .Take(count)
                        .ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }
}
