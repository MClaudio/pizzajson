﻿# PizzaApp
PizzaApp is a .NET Core Razor Web Application for displaying the top 20 most popular Pizza Configurations

## Requirements
[Visual Studio 2019](https://visualstudio.microsoft.com/vs/)

[.NET Core 3.1 SDK](https://dotnet.microsoft.com/download/dotnet/3.1)

## How to use
After cloning this Bitbucket repository, open the .sln file in Visual Studio.

In Visual Studio, build and run the solution.

The IIS Server will start up and display the coding exercise results in the browser.

### Notes
1. The main logic is found in the BrightwayApi.cs class
2. The controller for rendering the main page is found in Pages/Index.cshtml.cs
